<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.lang.*,servlet.*,main.*,configuration.*" %>
<%@ page import="com.mongodb.*" %>
<%@ page import="json.simple.*" %>
<%@ page import="gson.*" %>
<meta charset="utf-8">

<script src="https://d3plus.org/js/d3.js"></script>
<script src="https://d3plus.org/js/d3plus.js"></script>

<div id="viz"></div>

<form id="dataForm" >
	<input type="hidden" id="metric" value=""/>
	<input type="hidden" id="graph" value=""/>
	<input type="hidden" id="history" value=""/>
	<input type="hidden" id="system" value=""/>
	<input type="hidden" id="network" value=""/>
	<input type="hidden" id="request" value=""/>
	<input type="hidden" id="UA" value=""/>
	<input type="hidden" id="allUAs" value=""/>
</form>

<script>
  var data = [
    {"value": 100, "name": "alpha"},
    {"value": 70, "name": "beta"},
    {"value": 40, "name": "gamma"},
    {"value": 15, "name": "delta"},
    {"value": 5, "name": "epsilon"},
    {"value": 1, "name": "zeta"}
  ]
  d3plus.viz()
    .container("#viz")
    .data(data)
    .type("pie")
    .id("name")
    .size("value")
    .draw()
</script>