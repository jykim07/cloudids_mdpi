<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.lang.*,servlet.*,main.*,configuration.*" %>
<%@ page import="com.mongodb.*" %>
<%@ page import="json.simple.*" %>
<%@ page import="gson.*" %>

<html lang="en">

<head>
	<meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<meta http-equiv="pragma" content="no-cache" />
    <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://getbootstrap.com/examples/justified-nav/justified-nav.css" rel="stylesheet">


</head>

<body style="background:black;">
<script src="https://d3js.org/d3.v4.min.js"></script>
<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <link type="text/css" rel="stylesheet" href="http://mbostock.github.io/d3/talk/20111018/style.css"/>

<div id="tooltip"></div>

<style>

#tooltip {
	opacity: 0;
	position: fixed;			
	text-align: left;				
	padding: 5px;				
	font: 14px Verdana;
	color:#fff;		
	background: #333;	
	border: 0px;		
	border-radius: 2px;			
	z-index: 1099;
	overflow-y: scroll;
}

.node text {
  font: 13px Verdana;
}

</style>
<script src="https://d3js.org/d3.v3.min.js"></script>
<form id="dataForm" >
	<input type="hidden" id="metric" value=""/>
	<input type="hidden" id="graph" value=""/>
	<input type="hidden" id="history" value=""/>
	<input type="hidden" id="system" value=""/>
	<input type="hidden" id="network" value=""/>
	<input type="hidden" id="request" value=""/>
	<input type="hidden" id="UA" value=""/>
	<input type="hidden" id="allUAs" value=""/>
</form>

<script>

<%	
	webMain web = new webMain();
	
	String ip = request.getParameter("ip");
	ip = "\"" + ip + "\"" ;
%>

function ajaxdata(indexIP){
	var ajaxResult="";     
	document.getElementById("graph").value=indexIP;     

	var msgMetric=$('#metric').val();	
	var msgGraph=$('#graph').val(); 
	var msgHistory=$('#history').val(); 	
	var msgSystem=$('#system').val(); 
	var msgNetwork=$('#network').val();
	var msgRequest=$('#request').val();
	var msgUA=$('#UA').val();
	var msgAllUAs=$('#allUAs').val();

	$.ajax({
		'async': false,
		'url':'ActionServlet',
		'dataType':"json",
		'data':{metric:msgMetric,graph:msgGraph,history:msgHistory,system:msgSystem,network:msgNetwork,request:msgRequest,UA:msgUA,allUAs:msgAllUAs},
		'success':function(responseText){
			ajaxResult=responseText;
		}	
	});
	document.getElementById("graph").value=null;      
	return ajaxResult;
}


function ajaxMetric(indexIP){
	var ajaxResult="";     
	document.getElementById("metric").value=indexIP;     

	var msgMetric=$('#metric').val();	
	var msgGraph=$('#graph').val(); 
	var msgHistory=$('#history').val(); 	
	var msgSystem=$('#system').val(); 
	var msgNetwork=$('#network').val();
	var msgRequest=$('#request').val();
	var msgUA=$('#UA').val();
	var msgAllUAs=$('#allUAs').val();

	$.ajax({
		'async': false,
		'url':'ActionServlet',
		'dataType':"json",
		'data':{metric:msgMetric,graph:msgGraph,history:msgHistory,system:msgSystem,network:msgNetwork,request:msgRequest,UA:msgUA,allUAs:msgAllUAs},
		'success':function(responseText){
			ajaxResult=responseText;
		}	
	});
	document.getElementById("metric").value=null;      
	return ajaxResult;
}

var pageWidth = window.innerWidth,
    pageHeight = window.innerHeight;
    if (typeof pageWidth != "number"){
        if (document.compatMode == "CSS1Compat"){
            pageWidth = document.documentElement.clientWidth;
            pageHeight = document.documentElement.clientHeight;
        } else {
            pageWidth = document.body.clientWidth;
            pageHeight = document.body.clientHeight;
        }
	}
	
var width = pageWidth, 
	height = pageHeight, 
	MARGINS = {top: 20, right: 20, bottom: 20, left: 20};

var pcpMonitor = "http://13.58.41.194/#/?host=";
var metricPage = "metric.jsp?ip=";
var color = d3.scale.category20();

var vis = d3.select("body").append("svg:svg")
    .attr("width", width)
    .attr("height", height)
	.on("click", function(d){
			tooltip.style.opacity=0;
		});

var tooltip = document.getElementById('tooltip');
	
var indexIP=<%=ip%>;
console.log("ip" + indexIP);


var ua = ["normal","flooding","APIabuse","spoofing","bruteforce","ProtocolManipulation","injection","analysis","ModificationOfResources","SocialEngineering"];			

vis.append('text')
        .attr("class", "nodetext")
		.attr('x', MARGINS.left)
		.attr('y', MARGINS.top+10)
		.style('font-size',40)
		.style('stroke', "#33CCFF")
		.text('CLOUD IDS ');

vis.append('text')
        .attr("class", "nodetext")
		.attr('x', MARGINS.left)
		.attr('y', MARGINS.top+40)
		.style('font-size',15)
		.style('fill', "white")
		.style('stroke', "white")
		.text('- Supported Browser : Chrome -');
		
vis.append('text')
        .attr("class", "nodetext")
		.attr('x', MARGINS.top-5)
		.attr('y', height/4.5)
		.style('font-size',15)
		.style('stroke', "#F5B041")
		.style('fill', "#F5B041")
		.text('LEGEND :');
		
vis.append('text')
        .attr("class", "nodetext")
		.attr('x', MARGINS.top)
		.attr('y', height/4.5 + 20)
		.style('font-size',15)
		.style('stroke', "#F5B041")
		.style('fill', "#F5B041")		
		.text('SUSPICIOUS UNIT ATTACKS');
	
ua.forEach(function(d,i) {	   
   vis.append('circle')
           .attr("class", "nodetext")
   		.attr("cx", MARGINS.top)
		.attr("cy", height/4.5 + 38 + 30*i)
        .attr("r", 8)
        .attr("fill", function(d) { return color(i); })
		.style("stroke-width","1.5px")
		.style("stroke", "white");
		
   vis.append('text')
        .attr("class", "nodetext")
		.attr('x', MARGINS.top + 20)
		.attr('y', height/4.5 + 42 + 30*i)
		.style('font-size',15)	
		.style('fill', "white")			
		.style('stroke', "white")
		.text(d);
	});
	
var data = ajaxdata(indexIP);
   var force = self.force = d3.layout.force()
        .nodes(data.nodes)
        .links(data.links)
        .gravity(.03)
		.friction(0.7)
		.theta(0.6)
        .distance(160)
        .charge(-400)
        .size([width, height])
        .start();

    var link = vis.selectAll("line.link")
        .data(data.links)
        .enter().append("svg:line")
        .attr("class", "link")
		.style("strock-width", 3)
        .attr("x1", function(d) { return d.source.x; }).style("stroke", function(d) {return color(d.port);})
        .attr("y1", function(d) { return d.source.y; }).style("stroke", function(d) {return color(d.port);})
        .attr("x2", function(d) { return d.target.x; }).style("stroke", function(d) {return color(d.port);})
        .attr("y2", function(d) { return d.target.y; }).style("stroke", function(d) {return color(d.port);});
		
    var path = vis.append("g").selectAll("path")
            .data(data.links)
            .enter().append("svg:path")
            .style("stroke", function(d) {return color(d.port);})
            .style("stroke-width","2px")
			.attr("opacity","0.8")
            .attr("src", function (d) { return d.source.name; })
            .attr("dst", function (d) { return d.target.name; })
            .attr("marker-end", function(d) { return "url(#" + d.port + ")"; });
			
    var node_drag = d3.behavior.drag()
        .on("dragstart", dragstart)
        .on("drag", dragmove)
        .on("dragend", dragend);
			
	function linkArc(d) {
	  var dx = d.target.x - d.source.x,
		  dy = d.target.y - d.source.y,
		  dr = Math.sqrt(dx * dx + dy * dy);
	  return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;
	}		

    function dragstart(d, i) {
        force.stop() // stops the force auto positioning before you start dragging
    }

    function dragmove(d, i) {
        d.px += d3.event.dx;
        d.py += d3.event.dy;
        d.x += d3.event.dx;
        d.y += d3.event.dy; 
        tick(); // this is the key to make it work together with updating both px,py,x,y on d !
    }

    function dragend(d, i) {
        d.fixed = true; // of course set the node to fixed so the force doesn't include the node in its auto positioning stuff
        tick();
        force.resume();
    }
	
    var node = vis.selectAll("g.node")
        .data(data.nodes)
      .enter().append("svg:g")
        .attr("class", "node")
        .call(node_drag);

    node.append("svg:circle")
        .attr("class", "circle")
        .attr("r", 10)
        .attr("fill", function(d) { return color(d.group); })
		.attr("x", function(d){return d.x})
        .attr("y", function(d){return d.y})
		.attr("stroke", "white")
		.style("stroke-width", 1)
//		 .on("click", function loadPage(d){window.open(pcpMonitor + d.exIP,"_blank", "toolbar=yes,menubar=yes,resizable=yes,scrollbars=yes,width=" + width + ",height=" + height);});
		.on("mouseover", function(d){ // "<span style='font-style: newfont'>" + userinput + "</span>";				
			d3.select(this).transition().style("stroke-width","3px").style("stroke","orange");
			d3.select(this).style("cursor","pointer");
			tooltip.innerHTML = "External IP : " + '<span style="color:#F5B041">' + d.exIP + "</span>" + "<br>";
			tooltip.innerHTML += " Private IP : " + '<span style="color:#F5B041">' + d.name + "</span>" + "<br>";
			tooltip.innerHTML += "Unit Attack : " + '<span style="color:#F5B041">' + ua[d.group] + "</span>" + "<br><br>";
			
			var metric = ajaxMetric(d.name);
			
			for(var cnt =0; cnt < metric.length; cnt++)
			{	
				tooltip.innerHTML += metric[cnt].name + ' : ' + '<span style="color:#b6da57">' + metric[cnt].value + '</span>';
				if(metric[cnt].unit != '-')
					tooltip.innerHTML += '<span style="color:#b6da57"> / ' + metric[cnt].unit + '</span>';
				
				tooltip.innerHTML += "<br>";
			}			

			var left = width - width/4.5;
			var top = height / 20 ;

			tooltip.style.left = left + 'px';
			tooltip.style.top = top + 'px';
			tooltip.style.height = height - height/15;
			
			tooltip.style.opacity = 1;								
			
/*			var left = d3.event.pageX;
			var tooltipWidth =  parseInt(tooltip.style.width, 150);
			left = left - 180 ;
			
			tooltip.style.left = left + 'px';
			tooltip.style.top = (d3.event.pageY -5 ) + 'px';
			
			tooltip.style.opacity = 1;
*/		})
		.on("mouseout", function(d){
			d3.select(this).transition().style("stroke-width","1.5px").style("stroke","white");
		});
		 
    node.append("svg:text")
        .attr("class", "nodetext")
        .attr("dx", 20)
        .attr("dy", ".35em")
		.style("fill", "white")		
		.style("stroke", "white")
        .text(function(d) { return d.exIP+"_"+d.name });

	node.append("svg:text")
        .attr("class", "nodetext")
        .attr("dx", -20)
        .attr("dy", ".35em")
		.style("font-size", "15px")
		.style("fill", "#b6da57")		
		.style("stroke", "#b6da57")
        .text(">");
	
    node.append("svg:text")
        .attr("class", "nodetext")
        .attr("dx", 225)
        .attr("dy", ".35em")
		.style("font-size", "15px")
		.style("stroke", "#b6da57")
		.style("fill", "#b6da57")		
//		.style("text-decoration", "underline")
        .text("| more")
		.on("click", function loadPage(d){
			window.open(pcpMonitor + d.exIP,"_blank", "toolbar=yes,menubar=yes,resizable=yes,scrollbars=yes,width=" + width + ",height=" + height);
		})
		.on("mouseover", function(d){
			d3.select(this).style("cursor","pointer");		
		});
//		.on("click", function loadPage(d){window.open(metricPage + d.name,"_blank", "toolbar=yes,menubar=yes,resizable=yes,scrollbars=yes,width=" + width/1.2 + ",height=" + height/1.2);})
/*		.on("mouseover", function(d){
			var metric = ajaxMetric(d.name);
			
			tooltip.innerHTML = '';
			
			for(var cnt =0; cnt < metric.length; cnt++)
			{	
				tooltip.innerHTML += metric[cnt].name + ' : ' + metric[cnt].value;
				if(metric[cnt].unit != '-')
					tooltip.innerHTML += ' / ' + metric[cnt].unit;
				
				tooltip.innerHTML += "<br>";
			}
			var left = width - width/4.5;
			var top = height / 20 ;

			tooltip.style.left = left + 'px';
			tooltip.style.top = top + 'px';
			tooltip.style.height = height - height/15;
			
			tooltip.style.opacity = 1;	
		});
*/	

    force.on("tick", tick);

    function tick() {
//		path.attr("d", linkArc);		
      link.attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });
	  
      node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
    };

</script>
</body>
</html>
