<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.lang.*,servlet.*,main.*,configuration.*" %>
<%@ page import="com.mongodb.*" %>
<%@ page import="json.simple.*" %>
<%@ page import="gson.*" %>

<html lang="en">



<body style="background:white;">
	<script type="text/javascript" src="http://mbostock.github.com/d3/d3.js"></script>
	<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://d3plus.org/js/d3.js"></script>
	<script src="https://d3plus.org/js/d3plus.js"></script>
	

<div id="viz"></div>

<form id="dataForm" >
	<input type="hidden" id="metric" value=""/>
	<input type="hidden" id="graph" value=""/>
	<input type="hidden" id="history" value=""/>
	<input type="hidden" id="system" value=""/>
	<input type="hidden" id="network" value=""/>
	<input type="hidden" id="request" value=""/>
	<input type="hidden" id="UA" value=""/>
	<input type="hidden" id="allUAs" value=""/>
</form>

<style>
sbin.polol.css
    text.pieText {
      font-family: sans-serif;
      font-size: 12px;
      opacity: 0;
      
    }
    .legend {
      padding: 5px;
      font: 10px sans-serif;
    }
</style>
<script>

<%	
	webMain web = new webMain();
	
	String ip = request.getParameter("ip");
	ip = "\"" + ip + "\"" ;
%>


function ajaxdata(indexIP){
	var ajaxResult="";     
	document.getElementById("metric").value=indexIP;     

	var msgMetric=$('#metric').val();	
	var msgGraph=$('#graph').val(); 
	var msgHistory=$('#history').val(); 	
	var msgSystem=$('#system').val(); 
	var msgNetwork=$('#network').val();
	var msgRequest=$('#request').val();
	var msgUA=$('#UA').val();
	var msgAllUAs=$('#allUAs').val();

	$.ajax({
		'async': false,
		'url':'ActionServlet',
		'dataType':"json",
		'data':{metric:msgMetric,graph:msgGraph,history:msgHistory,system:msgSystem,network:msgNetwork,request:msgRequest,UA:msgUA,allUAs:msgAllUAs},
		'success':function(responseText){
			ajaxResult=responseText;
		}	
	});
	document.getElementById("metric").value=null;      
	return ajaxResult;
}

var pageWidth = window.innerWidth,
    pageHeight = window.innerHeight;
    if (typeof pageWidth != "number"){
        if (document.compatMode == "CSS1Compat"){
            pageWidth = document.documentElement.clientWidth;
            pageHeight = document.documentElement.clientHeight;
        } else {
            pageWidth = document.body.clientWidth;
            pageHeight = document.body.clientHeight;
        }
	}
	
var WIDTH = pageWidth/1.2, 
	HEIGHT = pageHeight/1.3, 
	MARGINS = {top: 20, right: 100, bottom: 30, left: 50};

var indexIP=<%=ip%>;
var data = ajaxdata(indexIP);
console.log(data);

d3plus.viz()
    .container("#viz")
    .data(data)
    .type("pie")
    .id("name")
    .size(1)
	.text({"value":"name"})
	.color({"scale":"category20c"})
	.tooltip({"value":["value","unit"],"share":false})

    .draw()

</script>
</body>
</html>