package process;

import configuration.Configuration;
import datatype.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class getMetric extends Thread
{
	Thread t;
	
    public getMetric()
	{		
		t = new SendMetric();
		t.start();
    }
}

class SendMetric extends Thread 
{
	private MetricInfo metric_info;
    
	public void run() 
	{
		String ip = Main.getSystemIP();
		this.metric_info = new MetricInfo(ip, Main.getExternalIP());		
		
		while(true)
		{
		    try 
		    {
				File readFile = new File(Configuration.getMetricUpdatedFile());
				
				if(readFile.exists() && readFile.length() > 0)
				{
					BufferedReader inFile = new BufferedReader(new FileReader(readFile));
					String sLine = null;
					
					while ((sLine = inFile.readLine()) != null) 
					{
						String[] oneLine = sLine.split(",");
						
						String name = oneLine[0];
						String option = oneLine[1];
						
						if(!name.contains("+*"))
						{
							name = name.substring(1); 
							if(name.equals("pmcd.pmlogger.port"))
								continue;
							
							this.metric_info.name = name; 		

							if(!option.equals(""))
								option = option.split("\"")[1];
							this.metric_info.option = option;
							
							this.metric_info.avg = Double.parseDouble(oneLine[2]);
							this.metric_info.min = Double.parseDouble(oneLine[3]);
							this.metric_info.max = Double.parseDouble(oneLine[4]);	
							this.metric_info.unit = oneLine[5];	
						}
 
						System.out.println("IP: " + ip
								+ ", name: " + this.metric_info.name + ", option: "
								+ this.metric_info.option + ", avg: " + this.metric_info.avg
								+ " , min " + this.metric_info.min + ", max: " + this.metric_info.max + ", unit :" + this.metric_info.unit 								
								);					

						Sendtoserver(Configuration.METRIC);								
					}
					inFile.close();
				}				
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			 
			try {
				Thread.sleep(Configuration.getSystemInterval());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
    }
	
	public void Sendtoserver(int infoType) 
	{
        RequestType request = new RequestType();
        request.SetIsXml(infoType);
		
		if (infoType == Configuration.METRIC) {
            request.SetMetricInfo(this.metric_info);
        }   
		
        Send2Server.Send(request);
    }
}
