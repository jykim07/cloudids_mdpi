#!/bin/bash  

user="ec2-user"

###### Update with the latest files first
DIR="/home/$user/cloudIDS/clientAgent"
cd $DIR/codes; 
cp -r cloudIDS.jar Config.txt runAgent.sh stopAgent.sh lib/ $DIR/deploy/files/

###### Send the files to VMs
IPlist="$DIR/deploy/scripts/iplist.txt"
IPs=$(cat $IPlist)
  
for i in $IPs  
do   

echo "========================="
echo $i

# for Root
#ssh $user@$i "mkdir -p /home/$user/cloudIDS"
#ssh $userR@$i "cd /usr/lib64; ln -s libpcap.so.1.5.3 libpcap.so.0.8"
#scp -r $DIR/deploy/files/* $userR@$i:/home/cloudSecurity/clientAgent
#scp -r $DIR/deploy/files/cloudSecurity.jar $userR@$i:/home/cloudSecurity/clientAgent
#scp -r $DIR/deploy/files/Config.txt $userR@$i:/home/cloudSecurity/clientAgent

# for ec2-user
#ssh $user@$i "cd /usr/lib64; sudo ln -s libpcap.so.1.5.3 libpcap.so.0.8"
#ssh $user@$i "mkdir -p /home/$user/cloudIDS"
scp -r $DIR/deploy/files/* $user@$i:/home/$user/cloudIDS
#scp -r $DIR/deploy/files/Config.txt $user@$i:/home/$user/cloudIDS
#scp -r $DIR/deploy/files/cloudIDS.jar $user@$i:/home/$user/cloudIDS

done
