#!/bin/bash  

user="ec2-user"
userR="root"

###### Run the program #####
DIR="/home/$user/cloudIDS/clientAgent"
IPlist="$DIR/deploy/scripts/iplist.txt"
IPs=$(cat $IPlist) 

RemoteDIR="/home/$user/cloudIDS"
#RemoteDIR="/home/cloudSecurity/clientAgent"
File="stopAgent.sh"

for i in $IPs  
do   
echo "** Run : "$i

ssh -t -t $user@$i "$RemoteDIR/$File " &
#ssh -t -t $userR@$i "$RemoteDIR/$File " &
done  
