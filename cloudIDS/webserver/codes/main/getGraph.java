package main;

import configuration.*;
import java.util.*;
import java.lang.*;
import java.text.SimpleDateFormat;
import com.mongodb.*;
import java.net.UnknownHostException;

public class getGraph
{
	static DBCollection collection;			
	
	static Map<String, pmInfo> pmGroup = new HashMap<String, pmInfo>();
	static ArrayList<String> setUA = new ArrayList<String>();
	
	static long oplogQueryTS = 0;  // 1000000000, 1430419645
	static long interval = Configuration.getInterval();
	static String data = "";
	

//db.graph.find({},{IP:1,exIP:1,LUA:1})
/*{ "_id" : ObjectId("5935b9582e1b51ecce453e1e"), "IP" : "172.31.2.109", "exIP" : "13.58.20.64", "LUA" : "bruteforce" }
{ "_id" : ObjectId("5935b9452e1b51ecce453e1d"), "IP" : "172.31.32.138", "exIP" : "52.15.161.5", "LUA" : "normal" }
*/

	public static long getNextQueryTS()
	{
		oplogQueryTS = System.currentTimeMillis() / 1000L;
		
		Date nextQueryTS = new Date((oplogQueryTS-interval)*1000L); 		
		long queryNextTS = (long)nextQueryTS.getTime()/1000;

		return queryNextTS;
	}		
	
	public getGraph(DB db, String collName) throws MongoException 
	{
		try 
		{
			collection = db.getCollection(collName);
			
//["normal", "flooding", "APIabuse", "spoofing", "bruteforce","ProtocolManipulation","injection","analysis", "SocialEngineering"]
			setUA.add("normal");
			setUA.add("flooding");
			setUA.add("APIabuse");
			setUA.add("spoofing");
			setUA.add("bruteforce");
			setUA.add("ProtocolManipulation");
			setUA.add("injection");
			setUA.add("analysis");
			setUA.add("ModificationOfResources");
			setUA.add("SocialEngineering");
		} 
		
		catch(MongoException e) {
			System.out.println(e.getMessage());
				e.printStackTrace();
		}	
	}
	
	public getGraph() {}

	
	public static String getResult() throws MongoException
	{
//		Map<String, pmInfo> pmGroup = new HashMap<String, pmInfo>();				
		
//		db.test.find({},{IP:1,exIP:1,LUA:1})
		BasicDBObject field = new BasicDBObject();
		BasicDBObject query = new BasicDBObject();

		query.put("IP",1);		
		query.put("exIP",1);
		query.put("LUA",1);

		Cursor cursor = collection.find(field,query);		
		
		while(cursor.hasNext())
		{
			BasicDBObject result = (BasicDBObject) cursor.next();	
			
			String ip = result.getString("IP");		
			String exIP = result.getString("exIP");	
			String LUA = result.getString("LUA");		
			
			pmInfo pm;
			if(!pmGroup.containsKey(ip))
				pm = new pmInfo();
			
			else
				pm = pmGroup.get(ip);
			
			pm.setExIP(exIP);
			pm.setLUA(LUA);
			
			String preLUA = pm.getPreLUA();
			
			if(preLUA!=null)
			{				
				if(!preLUA.equals("normal"))
				{
					pm.setLUA(preLUA);
				}
			}
			
			pm.setPreLUA(pm.getLUA());
			
			pmGroup.put(ip,pm); 		
		}
		
		Iterator<String> it = pmGroup.keySet().iterator();
		int count = 0;
		
		while(it.hasNext())
		{
			String ip = it.next();
			pmInfo pm = pmGroup.get(ip);
			
			pm.setIndex(count);
			
			count++;
		}
		
		System.out.println("=======YES");
		//db.test.aggregate({$unwind:"$MIN"},{$project:{"IP":"$IP","TS":"$MIN.RQ.neighbor.TS","Neigh":"$MIN.RQ.neighbor.address","Port":"$MIN.RQ.neighbor.port"}},{$match:{"TS":{$exists:true,$gte:"1496693080"}}})
		//db.graph.aggregate({$unwind:"$MIN"},{$project:{"IP":"$IP","TS":"$MIN.RQ.neighbor.TS","Neigh":"$MIN.RQ.neighbor.address","Port":"$MIN.RQ.neighbor.port"}},{$match:{"Neigh":{$exists:true}}})
			
		BasicDBObject unwind = new BasicDBObject("$unwind","$MIN");	
			
		query = new BasicDBObject();		
		query.put("IP","$IP");
		query.put("TS","$MIN.RQ.neighbor.TS");
		query.put("Port","$MIN.RQ.neighbor.port");
		query.put("Neigh","$MIN.RQ.neighbor.address");
			
		BasicDBObject project = new BasicDBObject("$project",query);	

		BasicDBObject match = new BasicDBObject("$match",new BasicDBObject("Neigh", new BasicDBObject("$exists","true")));
		List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();

		pipeline.add(unwind);
		pipeline.add(project);	
		pipeline.add(match);				
				
		AggregationOptions aggregationOptions = AggregationOptions.builder()
				.batchSize(100)
				.outputMode(AggregationOptions.OutputMode.CURSOR)
				.build();
			
		cursor = collection.aggregate(pipeline,aggregationOptions);	
			
		while(cursor.hasNext())
		{
			BasicDBObject result = (BasicDBObject) cursor.next();	
				
			String ip = result.getString("IP");	
			String TS = result.getString("TS");
			String neigh = result.getString("Neigh");	
			String port = result.getString("Port");
				
			pmInfo pm = pmGroup.get(ip);

			pm.setNeighbor(neigh);
			pm.setPort(port);
			pmGroup.put(ip, pm);	
		}			
		
		String result = pmGroupToJSON(pmGroup);
		
		return result;
	}
	
	public static String pmGroupToJSON(Map<String, pmInfo> pmGroup)
	{		
		String result = "{\"nodes\":[";
		Iterator<String> it = pmGroup.keySet().iterator();
		
		while (it.hasNext()) 
		{
			String ip = it.next();
			pmInfo pm = pmGroup.get(ip);
			
			String exIP = pm.getExIP();
			String LUA = pm.getLUA();
			
			if(LUA == null)
				LUA = "normal";		
			
			result += "{\"name\":\"" + ip + "\",\"exIP\":\"" + exIP + "\"," + "\"group\":" +  setUA.indexOf(LUA) + "}";
			
			if(it.hasNext())
				result += ",";
		}

		result += "],";
		result += "\"links\":[";
		
		it = pmGroup.keySet().iterator();		

		boolean pre = false;
		
		while (it.hasNext()) 
		{			
			String ip = it.next();
			pmInfo pm = pmGroup.get(ip);
			String port = pm.getPort();
			int pmIndex = pm.getIndex();
			
			HashSet<String> neighbor = pm.getNeighbor();
			Iterator<String> itTarget = neighbor.iterator();			
			System.out.println("## : " + ip);	

			if(!itTarget.hasNext())
			{
//				pre = false;
				continue;
			}
			
			while(itTarget.hasNext())
			{
				String target = itTarget.next();

				if(!pmGroup.containsKey(target))
					continue;
				
				pmInfo neighborInfo = pmGroup.get(target);
				int targetIndex = neighborInfo.getIndex();
				
				if(pre)
					result += ",";
				
				result += "{\"source\":" + pmIndex + ",\"target\":" + targetIndex + "," + "\"port\":" +  port + "}";

				System.out.println(pmIndex + " " + targetIndex + " " + " PRE : " + pre);
				
				pre = true;
/*				if(itTarget.hasNext())
				{
					result += ",";
					System.out.println("/1/");
				}
*/			}	
			
//			if(it.hasNext())

			
/*			if(!flag1)
	
				pre = true;
			
			else
			{
				if(pre)
				{
					result += ",";
					System.out.println("/2/");						
				}
				
				else if(it.hasNext())
				{
					pre = true;
					continue;					
				}
			}
*/		}
		
		result += "]}";
		return result;
	}
	
	static String getDate(int time)
	{
		long milliseconds = time * 1000L;
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(milliseconds);
	   
	   return sdf.format(cal.getTime());
	}
}

class pmInfo
{
	String exIP = null;
	String LUA = null;
	String preLUA = null;
	String portNum = null;
	int index = 0;
	
	HashSet<String> neighbor = new HashSet<String>();
	
	public void setNeighbor(String ip)
	{
		neighbor.add(ip);
	}
	
	public void setExIP(String ip)
	{
		exIP = ip;
	}

	public void setLUA(String ua)
	{
		LUA = ua;
	}	

	public void setPreLUA(String ua)
	{
		preLUA = ua;
	}
	
	public void setPort(String port)
	{
		portNum = port;
	}	
	
	public void setIndex(int index)
	{
		this.index = index;
	}
	
	public HashSet<String> getNeighbor()
	{
		return neighbor;
	}
	
	public String getExIP()
	{
		return exIP;
	}
	
	public String getLUA()
	{
		return LUA;
	}

	public String getPreLUA()
	{
		return preLUA;
	}
	
	public String getPort()
	{
		return portNum;
	}	
	
	public int getIndex()
	{
		return index;
	}		
}

