package main;

import java.util.*;
import java.lang.*;
import java.text.SimpleDateFormat;
import com.mongodb.*;
import java.net.UnknownHostException;

public class getMetric
{
	static DBCollection collection;			
	static String data = "";
	
	public getMetric(DB db, String collName) throws MongoException 
	{
		try 
		{
			collection = db.getCollection(collName);
		} 
		catch(MongoException e) 
		{
			System.out.println(e.getMessage());
				e.printStackTrace();
		}	
	}
	
	public getMetric() {}
	
	public static String getResult(String clickedIP, String dataType)
	{
		String data = "[";		
		Cursor cursor = getCursor(clickedIP);

		while(cursor.hasNext())
		{		
			BasicDBObject result = (BasicDBObject) cursor.next();	
			
			Set<String> keys = result.keySet();
			Iterator<String> it = keys.iterator();
			
			while (it.hasNext()) 
			{
				String key = it.next();
				if(key.equals("_id") || key.equals("IP"))
					continue;
					
				String valueStr = result.getString(key);
				String value = null;
				String unit = null;
				
				if(valueStr.contains("/"))
				{
					value = valueStr.split("/")[0];
					unit = valueStr.split("/")[1];
				}
				
				else
				{
					value = valueStr;	
					unit = "-";
				}
				
				System.out.println(value + " // " + unit);
				
				data += "{\"name\" : \"" + key + "\",";			
				data += "\"value\" : " + value + ",";
				data += "\"unit\" :  \"" + unit + "\"}";				

				if(it.hasNext())
					data += ",";	
			}
		}
		data += "]";	
		return data;
	}
	
	public static Cursor getCursor(String ip) throws MongoException
	{		
		ArrayList<String> monitorGroup = new ArrayList<String>();
		
		monitorGroup.add(ip);
		
// db.history.aggregate({$project:{"IP":"$IP","min":"$MIN","hour":"$HOUR"}},{$match:{'IP':{$in:["10.214.10.15","10.1.0.21"]}}})
/*		BasicDBObject query = new BasicDBObject();		
		query.put("IP","$IP");
		query.put("min","$MIN");

		BasicDBObject project = new BasicDBObject("$project",query);	
*/
		BasicDBObject match = new BasicDBObject("$match",new BasicDBObject("IP", new BasicDBObject("$in", monitorGroup)));
				
		List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();

//		pipeline.add(project);	
		pipeline.add(match);				
			
		AggregationOptions aggregationOptions = AggregationOptions.builder()
			.batchSize(100)
			.outputMode(AggregationOptions.OutputMode.CURSOR)
			.build();
		
		Cursor cursor = collection.aggregate(pipeline,aggregationOptions);	

		return cursor;
	}
}